<?php
/**
 * @file
 * wwu_responsive_majors.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wwu_responsive_majors_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wwu_responsive_majors_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wwu_responsive_majors_node_info() {
  $items = array(
    'major' => array(
      'name' => t('Major'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Major'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function wwu_responsive_majors_paragraphs_info() {
  $items = array(
    'majors_aside' => array(
      'name' => 'Aside',
      'bundle' => 'majors_aside',
      'locked' => '1',
    ),
    'majors_body_text' => array(
      'name' => 'Body Text',
      'bundle' => 'majors_body_text',
      'locked' => '1',
    ),
    'majors_pull_quote' => array(
      'name' => 'Pull Quote',
      'bundle' => 'majors_pull_quote',
      'locked' => '1',
    ),
  );
  return $items;
}
