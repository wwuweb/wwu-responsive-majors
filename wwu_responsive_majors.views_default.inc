<?php
/**
 * @file
 * wwu_responsive_majors.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wwu_responsive_majors_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'departments';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Departments';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'references_style';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'wwu_entity' => 'wwu_entity',
  );
  /* Filter criterion: Content: Entity Type (field_ent_type) */
  $handler->display->display_options['filters']['field_ent_type_tid']['id'] = 'field_ent_type_tid';
  $handler->display->display_options['filters']['field_ent_type_tid']['table'] = 'field_data_field_ent_type';
  $handler->display->display_options['filters']['field_ent_type_tid']['field'] = 'field_ent_type_tid';
  $handler->display->display_options['filters']['field_ent_type_tid']['value'] = array(
    54 => '54',
  );
  $handler->display->display_options['filters']['field_ent_type_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_ent_type_tid']['vocabulary'] = 'entity_type';

  /* Display: References */
  $handler = $view->new_display('references', 'References', 'references_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $export['departments'] = $view;

  return $export;
}
