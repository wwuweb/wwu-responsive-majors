<?php
/**
 * @file
 * wwu_responsive_majors.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function wwu_responsive_majors_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_degrees|node|major|form';
  $field_group->group_name = 'group_degrees';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'major';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Degrees',
    'weight' => '10',
    'children' => array(
      0 => 'field_courses_credits',
      1 => 'field_degree_info',
      2 => 'field_degree_courses',
      3 => 'field_courses_degree_select',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'classes' => ' group-degrees field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_degrees|node|major|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_majors_quote_author|paragraphs_item|majors_pull_quote|default';
  $field_group->group_name = 'group_majors_quote_author';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'majors_pull_quote';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Pull Quote Author',
    'weight' => '0',
    'children' => array(
      0 => 'field_majors_quote_author_name',
      1 => 'field_majors_quote_author_title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Pull Quote Author',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-majors-quote-author field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_majors_quote_author|paragraphs_item|majors_pull_quote|default'] = $field_group;

  return $export;
}
